﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RSSTestApp
{
    /// <summary>
    /// Interaction logic for ConfigPage.xaml
    /// </summary>
    public partial class ConfigPage : Window
    {
        public ConfigPage()
        {
            InitializeComponent();
            text_db_string.Text = ConfigurationManager.ConnectionStrings["DBConnectionString"].ToString();
        }
    }
}
