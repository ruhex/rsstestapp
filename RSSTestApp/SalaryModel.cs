﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RSSTestApp
{
    public class SalaryModel
    {
        public int Staff_id { get; set; }
        public decimal Salary { get; set; }
        public DateTime? Datetime { get; set; }

        static public List<SalaryModel> massiv = new List<SalaryModel>();

        public static List<SalaryModel> SelectAll()
        {
            massiv.Clear();

            IDataReader reader = Sql.Read("SELECT * FROM Salary");

            while (reader.Read())
            {
                massiv.Add(new SalaryModel
                {
                    Staff_id = (int)reader["Staff_id"],
                    Salary = (decimal)reader["Salary"],
                    Datetime = (DateTime)reader["Datetime"]
                });
            }
            reader.Close();
            return massiv;

        }
    }
}
