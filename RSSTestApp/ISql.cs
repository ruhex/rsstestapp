﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RSSTestApp
{
    public interface ISql
    {
        IDataReader Read(IDbConnection connection, IDbCommand command);
    }
}
