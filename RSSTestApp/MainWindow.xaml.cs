﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RSSTestApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : System.Windows.Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void listbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int PayParam;
            listbox.ItemsSource = null;
            bool? IsFared = IsFiredCheck.IsChecked;

            if (params_pay.SelectedItem != null)
            {
                string connectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ToString();
               
                using (Sql.connection = new SqlConnection(connectionString))
                {
                    
                    try
                    {
                        
                        Sql.connection.Open();
                        status_connect.Content = "Connection open!";

                        PayParam = params_pay.SelectedIndex;
                        
                        if (IsFared == false)
                            StaffModel.massiv = StaffModel.SelectActive();
                        else
                            StaffModel.massiv = StaffModel.SelectAll();

                        switch (PayParam)
                        {
                            case 0:
                                listbox.ItemTemplateSelector = new Selector(StaffModel.TypePay.Middle);
                                
                                StaffModel.massiv = StaffModel.Sort(StaffModel.GetPay(StaffModel.TypePay.Middle, StaffModel.massiv));
                                break;
                            case 1:
                                listbox.ItemTemplateSelector = new Selector(StaffModel.TypePay.Max);
                                StaffModel.massiv = StaffModel.Sort(StaffModel.GetPay(StaffModel.TypePay.Max, StaffModel.massiv));
                                break;
                        }
                        listbox.ItemsSource = StaffModel.massiv;
                    }
                    catch (SqlException error)
                    {
                        MessageBox.Show(error.Message);
                    }
                }
            }
            else
            {
                MessageBox.Show("Выберите один из параметров зарплаты", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void textboxFind_GotFocus(object sender, RoutedEventArgs e)
        {
            textboxFind.Text = null;
        }

        private void textboxFind_KeyUp(object sender, KeyEventArgs e)
        {
            listbox.ItemsSource = null;
            listbox.ItemsSource = StaffModel.SelectByName(textboxFind.Text);
        }

        private void ConfigWindow(object sender, RoutedEventArgs e)
        {
            new ConfigPage().Show();
        }
    }
}
