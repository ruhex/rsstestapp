﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace RSSTestApp
{
    class Selector : DataTemplateSelector
    {
        private StaffModel.TypePay Type { get; set; }

        public Selector(StaffModel.TypePay type)
        {
            Type = type;
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            StaffModel staff = (StaffModel)item;
            FrameworkElement element = container as FrameworkElement;

            if (Type == StaffModel.TypePay.Max)
            {
                if (staff.Pay <= (decimal)20000)
                    return element.FindResource("DataTemplateSelectMax20000") as DataTemplate;
                else
                    return element.FindResource("DataTemplateSelectMaxPay") as DataTemplate;
            }

            if (Type == StaffModel.TypePay.Middle)
            {
                if (staff.Pay <= (decimal)20000)
                    return element.FindResource("DataTemplateSelectMiddle20000") as DataTemplate;
                else
                    return element.FindResource("DataTemplateSelectMiddlePay") as DataTemplate;
            }
            return null;
        }
    }
}
