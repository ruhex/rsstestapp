﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RSSTestApp
{
    public class StaffModel
    {
        public int id { get; set; }
        public string Name { get; set; }
        private bool Active { get; set; }

        public string ActiveToString
        {
            get { return Active == true ? "Работает" : "Не работает"; }
        }

        public enum TypePay { Max, Middle };

        // this parameters need for data binding
        private decimal MaxPay { get; set; }
        private decimal MiddlePay { get; set; }

        public decimal Pay { get; set; }

        public List<SalaryModel> Salatys = new List<SalaryModel>();
        static public List<StaffModel> massiv = new List<StaffModel>();

        public static List<StaffModel> SelectActive()
        {
            return Select("SELECT * FROM Staff Where Staff.Active = 1");
        }

        public static List<StaffModel> SelectAll()
        {
            return Select("SELECT * FROM Staff");
        }

        private static List<StaffModel> Select(string sql)
        {
            
            massiv.Clear();
            int tmp_id;
            List<SalaryModel> tmp_massiv = new List<SalaryModel>();
            IDataReader reader = Sql.Read(sql);

            while (reader.Read())
            {
                tmp_id = (int)reader["id"];
                tmp_massiv.Clear();
                tmp_massiv = SalaryModel.SelectAll();
                massiv.Add(new StaffModel
                {
                    id = tmp_id,
                    Name = (string)reader["Name"],
                    Active = (bool)reader["Active"],
                    Salatys = SelectSalary(tmp_id, tmp_massiv)
                    //MaxPay = GetMaxPay(SelectSalary(tmp_id, tmp_massiv)),
                    //MiddlePay = GetMiddlePay(SelectSalary(tmp_id, tmp_massiv))
                });
            }
            //reader.Close();

            //foreach (StaffModel staff in massiv)
            //    staff.Salatys = SelectSalary(staff, SalaryModel.SelectAll());
            return massiv;

        }
        public static List<SalaryModel> SelectSalary(int user_id, List<SalaryModel> massiv)
        {
            return massiv.FindAll(delegate (SalaryModel salary) { return salary.Staff_id == user_id; });
        }

        public static List<StaffModel> SelectByName(string name)
        {
            return massiv.FindAll(delegate (StaffModel staff) { return staff.Name.ToUpper() == name.ToUpper(); });

            #region for search part name
            //for (int i = 0; i < name.Length; i++)
            //{
            //    for (int j = 0; j < name.Length; j++)
            //    {

            //    }
            //}

            //return massiv.FindAll(delegate (StaffModel staff)
            //{
            //    //return staff.Name.ToUpper()[j] == name.ToUpper()[i];
            //    int i;
            //    int j;
            //    int p;
            //    staff.Name = NullTermStr(staff.Name);
            //    name = NullTermStr(name);
            //    p = 1;
            //    i = 0;

            //    while (null_name1[i] != '\0')
            //    {
            //        j = 0;
            //        while (null_name2[j] != '\0')
            //        {
            //            if (null_name1[i] == null_name2[j])
            //            {
            //                p++;
            //                i++;
            //            }
            //            else
            //                i++;
            //            j++;
            //        }
            //    }



            //    if (p == name.Length)
            //        return true;
            //    return false;

            //});
            #endregion

        }

        private static string NullTermStr(string str)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(str);
            sb.Append('\0');
            return sb.ToString();
        }

        public static List<SalaryModel> SelectSalary(StaffModel user, List<SalaryModel> massiv)
        {
            return massiv.FindAll(delegate (SalaryModel salary) { return salary.Staff_id == user.id; });
        }

        // pay in month MAX 

        public static List<StaffModel> GetPay(TypePay type, List<StaffModel> massiv)
        {
            switch (type)
            {
                case TypePay.Max:
                    foreach (StaffModel staff in massiv)
                        staff.Pay = GetMaxPay(staff.Salatys);
                    break;
                   
                case TypePay.Middle:
                    foreach (StaffModel staff in massiv)
                        staff.Pay = GetMiddlePay(staff.Salatys);
                    break;
            }
            return massiv;
        }
        private static decimal GetMaxPay(List<SalaryModel> salaries)
        {
            int i;
            decimal tmp;

            tmp = 0;
            i = -1;
            if (salaries == null || salaries.Count == 0)
                return 0;

            if (salaries.Count == 1)
                return salaries[0].Salary;

            while (++i < salaries.Count - 1)
            {
                DateTime? time = salaries[i].Datetime;
                if (time.Value.Month == salaries[i + 1].Datetime.Value.Month)
                {
                    tmp += (salaries[i].Salary + salaries[i + 1].Salary);
                }
                else
                    tmp = salaries[i].Salary;
            }

            foreach (SalaryModel item in salaries)
            {
                if (item.Salary > tmp)
                    return item.Salary;
            }
            return tmp;
        }

        private static decimal GetMiddlePay(List<SalaryModel> salaries)
        {
            int count;
            int i;
            decimal test;

            if (salaries == null)
                return 0;

            test = 0;
            foreach (SalaryModel item in salaries)
                test += item.Salary;
            count = 0;
            i = -1;
            if (salaries.Count == 0)
                return 0;

            while (++i < salaries.Count - 1)
            {
                DateTime? time = salaries[i].Datetime;
                if (time.Value.Month == salaries[i + 1].Datetime.Value.Month)
                {
                    count++;
                }
            }


            return (test / (salaries.Count - count));


        }

        public static List<StaffModel> Sort(List<StaffModel> massiv)
        {
            for (int i = 0; i < massiv.Count; i++)
            {
                for (int j = 0; j < massiv.Count; j++)
                {
                    if (massiv[i].Pay > massiv[j].Pay)
                    {
                        decimal tmp;

                        tmp = massiv[i].Pay;
                        massiv[i].Pay = massiv[j].Pay;
                        massiv[j].Pay = tmp;
                    }
                }
            }

            return massiv;
        }

        public static List<StaffModel> SortMaxPay(List<StaffModel> massiv)
        {
            
            for (int i = 0; i < massiv.Count; i++)
            {
                for (int j = 0; j < massiv.Count; j++)
                {
                    if (massiv[i].MaxPay > massiv[j].MaxPay)
                    {
                        decimal tmp;

                        tmp = massiv[i].MaxPay;
                        massiv[i].MaxPay = massiv[j].MaxPay;
                        massiv[j].MaxPay = tmp;
                    }
                }
            }

            return massiv;
        }
    }
}
