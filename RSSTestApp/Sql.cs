﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Configuration;

namespace RSSTestApp
{
    static public class Sql 
    {
        //private static Sql _instance { get; set; }   
        static public IDataReader reader { get; set; }
        static public IDbConnection connection { get; set; }
        static public string CommandText { get; set; }
        static private IDbCommand command;
        //private string ConnectionString { get; set; }

        //public Sql(string connectionString)
        //{
        //    this.ConnectionString = connectionString;
        //}
        static public IDataReader Read(IDbConnection connection, IDbCommand command)
        {
            
            command.Connection = connection ;
            return command.ExecuteReader();
        }

        static public IDataReader Read(string query)
        {
            command = new SqlCommand(query);
            command.Connection = connection;
            return command.ExecuteReader();
        }

        //public static Sql GetInstance(string connectionString)
        //{
        //    if (_instance == null)
        //        _instance = new Sql();
        //    return _instance;
        //}

    }
}
